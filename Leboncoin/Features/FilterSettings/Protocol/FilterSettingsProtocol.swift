//
//  FilterSettingsProtocol.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

protocol FilterSettingsViewControllerProtocol: class {}

protocol FilterSettingsPresenterProtocol {
    func getNumberOfRows() -> Int
    func getFilter(at index: Int) -> AdCategory?
    func getFiltersEnabled() -> [AdCategory]?
    func isFilterEnabled(for category: AdCategory?) -> Bool
    func didSelectItem(at index: Int) -> AdCategory?
    func applyFilters()
}
