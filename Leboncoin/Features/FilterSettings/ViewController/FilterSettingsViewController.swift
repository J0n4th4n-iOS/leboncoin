//
//  FilterSettingsViewController.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation
import UIKit

class FilterSettingsViewController: UITableViewController {
    // MARK: properties
    var presenter: FilterSettingsPresenterProtocol?
    
    // MARK: Assemble
    static func assemble(with delegate: HomeViewSettingsDelegate, filters: [AdCategory]) -> FilterSettingsViewController {
        let filterSettingsViewController = FilterSettingsViewController()
        filterSettingsViewController.presenter = FilterSettingsPresenter(view: filterSettingsViewController, delegate: delegate ,filters: filters)
        return filterSettingsViewController
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "filterTitle".localized
        addNavigationBarButtonItem()
        configureTableView()
    }
}

extension FilterSettingsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.getNumberOfRows() ?? .zero
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.adCategoryCellIdentifier.rawValue, for: indexPath)
        let filter = presenter?.getFilter(at: indexPath.row)
        cell.configure(with: UITableViewCell.SelectionStyle.none,
                       textColor: .whiteText,
                       font: .boldSystemFont(ofSize: Constants.FontSize.title),
                       backgroundColor: presenter?.isFilterEnabled(for: filter) ?? false ? .blueApp : . darkGray,
                       text: filter?.title)
        
        return cell
    }
}

extension FilterSettingsViewController {
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)
        let filter = presenter?.didSelectItem(at: indexPath.row)
        selectedCell?.backgroundColor = filter != nil ? .blueApp : .darkGray
    }
}

extension FilterSettingsViewController: FilterSettingsViewControllerProtocol {
    
}

extension FilterSettingsViewController {
    // MARK: Private methods
    private func configureTableView() {
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .grayApp
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ReuseIdentifier.adCategoryCellIdentifier.rawValue)
    }
    
    private func addNavigationBarButtonItem() {
        let filterButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onValidateFiltersButton))
        filterButtonItem.tintColor = .white
        self.navigationItem.rightBarButtonItem  = filterButtonItem
    }
    
    @objc private func onValidateFiltersButton() {
        presenter?.applyFilters()
        navigationController?.popViewController(animated: true)
    }
}
