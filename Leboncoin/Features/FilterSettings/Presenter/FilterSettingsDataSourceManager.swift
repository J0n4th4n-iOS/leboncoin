//
//  FilterSettingsDataSourceManager.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

protocol FilterSettingsDataSourceManagerProtocol {
    var filters: [AdCategory]? { get }
    func getNumberOfRows() -> Int
    func getFilter(at index: Int) -> AdCategory?
    func isFilterEnabled(for category: AdCategory?) -> Bool
    func filterSelected(at index: Int) -> AdCategory?
}

class FilterSettingsDataSourceManager {
    // MARK: Properties
    var filters: [AdCategory]?
    
    // MARK: Init
    init(with filters: [AdCategory]?) {
        self.filters = filters
    }
}

extension FilterSettingsDataSourceManager: FilterSettingsDataSourceManagerProtocol {
    // MARK: FilterSettingsDataSourceManagerProtocol
    func getNumberOfRows() -> Int {
        AdCategory.allCases.count
    }
    
    func getFilter(at index: Int) -> AdCategory? {
        AdCategory.getCategory(for: index)
    }
    
    func isFilterEnabled(for category: AdCategory?) -> Bool {
        guard let category = category else { return false }
        
        return filters?.contains(category) ?? false
    }
    
    func filterSelected(at index: Int) -> AdCategory? {
        guard let filter = getFilter(at: index) else { return nil }
        return filters?.shouldAddOnSelection(filter) ?? false ? filter : nil
    }
}
