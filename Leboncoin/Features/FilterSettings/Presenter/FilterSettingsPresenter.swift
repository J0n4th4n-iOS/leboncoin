//
//  FilterSettingsPresenter.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

class FilterSettingsPresenter {
    // MARK: Properties
    weak var view: FilterSettingsViewControllerProtocol?
    private weak var delegate: HomeViewSettingsDelegate?
    var dataSourceManager: FilterSettingsDataSourceManagerProtocol?

    // MARK: Init
    init(view: FilterSettingsViewControllerProtocol?,
         delegate: HomeViewSettingsDelegate?,
         filters: [AdCategory]?) {
        self.view = view
        self.delegate = delegate
        self.dataSourceManager = FilterSettingsDataSourceManager(with: filters)
    }
}

extension FilterSettingsPresenter: FilterSettingsPresenterProtocol {
    // MARK: FilterSettingsPresenterProtocol
    func getNumberOfRows() -> Int {
        dataSourceManager?.getNumberOfRows() ?? .zero
    }
    
    func getFilter(at index: Int) -> AdCategory? {
        dataSourceManager?.getFilter(at: index)
    }
    
    func getFiltersEnabled() -> [AdCategory]? {
        dataSourceManager?.filters
    }
    
    func isFilterEnabled(for category: AdCategory?) -> Bool {
        dataSourceManager?.isFilterEnabled(for: category) ?? false
    }
    
    func didSelectItem(at index: Int) -> AdCategory? {
        dataSourceManager?.filterSelected(at: index)
    }
    
    func applyFilters() {
        delegate?.applyFilters(getFiltersEnabled() ?? [])
    }
}
