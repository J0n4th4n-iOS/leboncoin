//
//  DetailsPresenter.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 10/09/2021.
//

import Foundation

class DetailsPresenter {
    // MARK: Properties
    weak var view: DetailsViewProtocol?
    private var ad: Ad?
    
    // MARK: Init
    public init(with view: DetailsViewProtocol?, ad: Ad) {
        self.view = view
        self.ad = ad
    }
}

extension DetailsPresenter: DetailsPresenterProtocol {
    func didLoad() {
        view?.showDetails(ad: ad)
    }
}
