//
//  DetailsProtocol.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 10/09/2021.
//

import Foundation

protocol DetailsViewProtocol: class {
    func showDetails(ad: Ad?)
}

protocol DetailsPresenterProtocol {
    func didLoad()
}
