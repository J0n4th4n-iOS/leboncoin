//
//  DetailsViewController.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 10/09/2021.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController {
    // MARK: Properties
    private var presenter: DetailsPresenterProtocol?
    
    private let scrollView: UIScrollView = {
        UIScrollView()
    }()
    
    private let contentView: UIView = {
        UIView()
    }()
    private let imageAdView: UIImageView = {
        let imageView = UIImageView()
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private let urgentView: CapsuleView = {
        CapsuleView()
    }()
    
    private let titleLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.title),
                textColor: .whiteText,
                numberOfLines: .zero)
    }()
    
    private let priceLabel: UILabel = {
        UILabel(with: .systemFont(ofSize: Constants.FontSize.body),
                textColor: .whiteText,
                numberOfLines: .zero)
    }()
    
    private let dateLabel: UILabel = {
        UILabel(with: .systemFont(ofSize: Constants.FontSize.body),
                textColor: .whiteText)
    }()
    
    private let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = .backgroundView
        return view
    }()
    
    private let descriptionLabel: UILabel = {
        UILabel(with: UIFont.systemFont(ofSize: Constants.FontSize.body),
                textColor: .whiteText,
                textAlignment: .justified,
                numberOfLines: .zero)
    }()
    
    //MARK: Assemble
    static func assemble(with ad: Ad) -> DetailsViewController {
        let detailsViewController = DetailsViewController()
        detailsViewController.presenter = DetailsPresenter(with: detailsViewController, ad: ad)
        return detailsViewController
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildUI()
        presenter?.didLoad()
    }
}

extension DetailsViewController: DetailsViewProtocol {
    // MARK: DetailsViewProtocol
    func showDetails(ad: Ad?) {
        title = ad?.categoryId?.title
        
        imageAdView.load(from: ad?.images?.thumb, contentMode: .scaleAspectFill)
        configureIsUrgentLabel(isUrgent: ad?.isUrgent ?? false)
        titleLabel.text = ad?.title
        priceLabel.text = ad?.price?.amountFormat()?.deviseSuffix
        dateLabel.text = ad?.creationDate?.toDate(inputFormat: .defaultTimeZone, outputFormat: .dayAndMonthWithHours)
        descriptionLabel.text = ad?.description?.prefix(with: "description".localized)
    }
}

extension DetailsViewController {
    // MARK: Private methods
    private func buildUI() {
        view.backgroundColor = .grayApp
        
        scrollView.showIn(view: view, safeAreaLayoutGuideEnabled: (true, true, true, true))
        
        contentView.anchor(in: scrollView,
                           top: scrollView.topAnchor,
                           bottom: scrollView.bottomAnchor,
                           leading: scrollView.leadingAnchor,
                           trailing: scrollView.trailingAnchor)
        contentView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
        
        imageAdView.anchor(in: contentView,
                           top: contentView.topAnchor,
                           leading: contentView.leadingAnchor,
                           trailing: contentView.trailingAnchor,
                           width: contentView.frame.width)
        imageAdView.heightAnchor.constraint(equalTo: scrollView.heightAnchor,
                                            multiplier: Constants.Size.halfRatio).isActive = true
        
        
        let stackView = UIStackView(arrangedSubviews: [urgentView, titleLabel, priceLabel, dateLabel, separatorView, descriptionLabel])
        stackView.configure(alignment: .leading, spacing: Constants.Margin.mediumMargin)
        
        stackView.anchor(in: contentView,
                         top: imageAdView.bottomAnchor, paddingTop: Constants.Margin.largeMargin,
                         bottom: contentView.bottomAnchor, paddingBottom: Constants.Margin.largeMargin,
                         leading: contentView.leadingAnchor, paddingLeft: Constants.Margin.largeMargin,
                         trailing: contentView.trailingAnchor, paddingRight: Constants.Margin.largeMargin)
        
        separatorView.heightAnchor.constraint(equalToConstant: Constants.Size.separator).isActive = true
        separatorView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    }
    
    private func configureIsUrgentLabel(isUrgent: Bool) {
        urgentView.isHidden = !isUrgent
        urgentView.text = "urgent".localized
    }
}
