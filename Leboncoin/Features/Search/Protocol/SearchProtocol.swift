//
//  SearchProtocol.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

protocol SearchViewProtocol: class {
    func reloadData(for state: SearchState)
    func showDetails(for ad: Ad)
    func restoreSearch(with keywords: String?)
}

protocol SearchPresenterProtocol {
    func configure(_ cell: SearchCellProtocol, atIndexPath indexPath: IndexPath)
    func numberOfRows(atSection section: Int) -> Int
    func didSelectItem(at indexPath: IndexPath)
    func searchAds(with keywords: String?)
    func clearResults()
    func shouldRestoreSearch(with keywords: String?)
}
