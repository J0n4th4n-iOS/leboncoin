//
//  SearchPresenter.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

class SearchPresenter {
    // MARK: Properties
    weak var view: SearchViewProtocol?
    var dataSourceManager: SearchDataSourceManagerProtocol?
    
    // MARK: Init
    public init(view: SearchViewProtocol?, ads: [Ad]) {
        self.view = view
        self.dataSourceManager = SearchDataSourceManager(with: ads)
    }
}

extension SearchPresenter: SearchPresenterProtocol {
    // MARK: SearchPresenterProtocol

    func configure(_ cell: SearchCellProtocol, atIndexPath indexPath: IndexPath) {
        cell.configure(with: dataSourceManager?.getAd(for: indexPath.row))
    }
    
    func numberOfRows(atSection section: Int) -> Int {
        dataSourceManager?.searchResults?.count ?? 0
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        guard let ad = dataSourceManager?.searchResults?.getOrNull(indexPath.row) else { return }
        view?.showDetails(for: ad)
    }
    
    func searchAds(with keywords: String?) {
        dataSourceManager?.searchAds(with: keywords)
        view?.reloadData(for: dataSourceManager?.searchResults?.isEmpty ?? true ? .empty : .found)
    }
    
    func clearResults() {
        dataSourceManager?.clearSearchResults()
        view?.reloadData(for: .editing)
    }
    
    func shouldRestoreSearch(with keywords: String?) {
        view?.restoreSearch(with: dataSourceManager?.lastSearch)
    }
}
