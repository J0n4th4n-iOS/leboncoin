//
//  SearchDataSourceManager.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

protocol SearchDataSourceManagerProtocol {
    var searchResults: [Ad]? { get }
    var lastSearch: String? { get }
    func getAd(for index: Int) -> Ad?
    func searchAds(with keywords: String?) //Store keywords in lastsearch
    func clearSearchResults()
}

class SearchDataSourceManager {
    // MARK: Properties
    var ads: [Ad]?
    var searchResults: [Ad]? = []
    var lastSearch: String?
    
    // MARK: Init
    init(with ads: [Ad]?) {
        self.ads = ads
    }
}

extension SearchDataSourceManager: SearchDataSourceManagerProtocol {
    // MARK: SearchDataSourceManagerProtocol
    func getAd(for index: Int) -> Ad? {
        searchResults?.getOrNull(index)
    }

    func searchAds(with keywords: String?) {
        filterAds(with: keywords)
    }
    
    func clearSearchResults() {
        searchResults?.removeAll()
    }
}

extension SearchDataSourceManager {
    // MARK: Private methods
    private func filterAds(with searchText: String?) {
        lastSearch = searchText
        searchResults = ads?.filter { isSearchResult(for: $0, with: searchText) }
    }
    
    private func isSearchResult(for ad: Ad, with searchText: String?) -> Bool {
        guard let searchText = searchText else { return false }
        let searchTextNeutral = searchText.neutral
        return ad.title?.neutral.contains(searchTextNeutral) ?? false ||
            ad.description?.neutral.contains(searchTextNeutral) ?? false ||
            ad.categoryId?.title.neutral.contains(searchTextNeutral) ?? false
    }
}
