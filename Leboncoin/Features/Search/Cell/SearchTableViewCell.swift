//
//  SearchTableViewCell.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

protocol SearchCellProtocol {
    func configure(with ad: Ad?)
}

class SearchTableViewCell: UITableViewCell {
    // MARK: Properties
    private let adImageView: UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.customize(borderColor: .white,
                        borderWidth: Constants.BorderSize.adCell,
                        cornerRadius: Constants.Radius.searchCell)
        return image
    }()
    
    private let titleAdLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.title), numberOfLines: .zero)
    }()
    
    private let priceAdLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.body))
    }()
    
    private let categoryLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.footNote))
    }()
    
    private let priorityAdImageView: UIImageView = {
        UIImageView(image: UIImage(named: "urgent"))
    }()
    
    // MARK: Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension SearchTableViewCell: SearchCellProtocol {
    // MARK: SearchCellProtocol
    func configure(with ad: Ad?) {
        titleAdLabel.text = ad?.title
        priceAdLabel.text = ad?.price?.amountFormat()?.deviseSuffix
        categoryLabel.text = ad?.categoryId?.title
        adImageView.load(from: ad?.images?.small, contentMode: .scaleAspectFill)
        configureUrgentMode(ad?.isUrgent ?? false)
    }
}

extension SearchTableViewCell {
    // MARK: Private methods
    private func buildUI() {
        backgroundColor = .grayApp
         
        // Ad image
        adImageView.anchor(in: contentView,
                           top: contentView.topAnchor, paddingTop: Constants.Margin.mediumMargin,
                           bottom: contentView.bottomAnchor, paddingBottom: Constants.Margin.mediumMargin,
                           leading: contentView.leadingAnchor, paddingLeft: Constants.Margin.mediumMargin)
        adImageView.heightAnchor.constraint(equalToConstant: UIScreen.main.bounds.width/3).isActive = true
        adImageView.widthAnchor.constraint(equalTo: adImageView.heightAnchor).isActive = true
        
        // Ad description
        let descriptionStackView = UIStackView(arrangedSubviews: [titleAdLabel, priceAdLabel, categoryLabel])
        descriptionStackView.configure(spacing: Constants.Margin.minimumMargin)
        
        let stackView = UIStackView(arrangedSubviews: [adImageView, descriptionStackView])
        stackView.configure(with: .horizontal, alignment: .center, spacing: Constants.Margin.mediumMargin)
        
        let backgroundView = UIView()
        backgroundView.customize(with: .darkGray,
                                 borderColor: .white,
                                 borderWidth: Constants.BorderSize.adCell,
                                 cornerRadius: Constants.Radius.searchCell)
        
        stackView.showIn(view: backgroundView)
        backgroundView.showIn(view: contentView, withSpacing: Constants.Margin.largeMargin)
        
        // Urgent flag
        priorityAdImageView.anchor(in: contentView, top: adImageView.topAnchor,
                                   leading: adImageView.leadingAnchor, paddingLeft: Constants.Margin.largeMargin,
                                   width: Constants.Size.urgentImageViewWidth, height: Constants.Size.urgentImageViewHeight)
    }
    
    private func configureUrgentMode(_ isUrgent: Bool) {
        priorityAdImageView.isHidden = !(isUrgent)
    }
}
