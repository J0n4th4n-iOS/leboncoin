//
//  SearchViewController+TableView.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

extension SearchViewController {
    func configureTableView() {
        tableView?.separatorStyle = .none
        tableView?.backgroundColor = .grayApp
        tableView.estimatedRowHeight = Constants.Size.estimatedHeightCell
        tableView.rowHeight = UITableView.automaticDimension
        tableView.keyboardDismissMode = .onDrag
        tableView?.register(SearchTableViewCell.self,
                            forCellReuseIdentifier: ReuseIdentifier.searchCellIdentifier.rawValue)
        refreshTableView(forState: .editing)
    }
    
    func refreshTableView(forState state: SearchState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            state == .found ? self.tableView.restore() :
                self.tableView?.setBackgroundView(with: state.message, image: state.image)
        }
        tableView.reloadData()
        
    }
}

extension SearchViewController {
    // MARK: UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.numberOfRows(atSection: section) ?? .zero
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.searchCellIdentifier.rawValue, for: indexPath) as? SearchTableViewCell
        else {
            return UITableViewCell()
        }
        
        presenter?.configure(cell, atIndexPath: indexPath)
        return cell
    }
}

extension SearchViewController {
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.didSelectItem(at: indexPath)
    }
}
