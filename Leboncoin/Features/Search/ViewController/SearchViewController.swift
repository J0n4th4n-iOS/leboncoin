//
//  SearchViewController.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

class SearchViewController: UITableViewController {
    // MARK: Properties
    var presenter: SearchPresenterProtocol?
    lazy var searchBar = UISearchBar(frame: .zero)
    
    // MARK: Assemble
    static func assemble(ads: [Ad]) -> SearchViewController {
        let searchViewController = SearchViewController()
        searchViewController.presenter = SearchPresenter(view: searchViewController, ads: ads)
        return searchViewController
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        buildUI()
    }
}

extension SearchViewController: SearchViewProtocol {
    // MARK: SearchViewProtocol
    func reloadData(for state: SearchState) {
        refreshTableView(forState: state)
    }
    
    func showDetails(for ad: Ad) {
        Router.showFeature(.details(ad: ad), from: self)
    }
    
    func restoreSearch(with keywords: String?) {
        restoreSearchBar(with: keywords)
    }
}

extension SearchViewController {
    // MARK: Private methods
    private func buildUI() {
        navigationController?.clearBackButtonTitle()
        view.backgroundColor = .grayApp
        
        buildSearchComponent()
        configureTableView()
    }
}


