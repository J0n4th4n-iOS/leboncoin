//
//  SearchViewController+Search.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

extension SearchViewController {
    // MARK: Build search
    func buildSearchComponent() {
        searchBar.delegate = self
        searchBar.placeholder = "searchAds".localized
        searchBar.becomeFirstResponder()
        searchBar.setShowsCancelButton(true, animated: true)

        navigationItem.titleView = searchBar
    }
    
    func restoreSearchBar(with keywords: String?) {
        searchBar.text = keywords
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text?.isEmpty ?? true ? presenter?.clearResults() : presenter?.searchAds(with: searchBar.text)
        searchBar.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        presenter?.shouldRestoreSearch(with: searchBar.text)
    }
}
