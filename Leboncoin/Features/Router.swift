//
//  Router.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation
import UIKit

enum PresentMode {
    case push
    case modal(modalPresentationStyle: UIModalPresentationStyle)
}

enum FeatureType {
    case details(ad: Ad), filterSettings(delegate: HomeViewSettingsDelegate, filtersEnabled: [AdCategory]), listItems, search(ads: [Ad])
}

class Router {
    public static func showFeatureAsRoot(_ featureType: FeatureType,
                                         isNavigationControllerRequired: Bool = false,
                                         on window: UIWindow?) {
        let rootViewController = isNavigationControllerRequired ?
            UINavigationController(rootViewController: getAssembledFeature(featureType)) :
            getAssembledFeature(featureType)
        window?.rootViewController = rootViewController
    }
    
    public static func showFeature(_ featureType: FeatureType,
                                   from viewController: UIViewController,
                                   isNavigationControllerRequired: Bool = false,
                                   presentMode: PresentMode = .push,
                                   isAnimated: Bool = true) {
        let featureViewController =  isNavigationControllerRequired ?
            UINavigationController(rootViewController: getAssembledFeature(featureType)) :
            getAssembledFeature(featureType)
        
        switch presentMode {
        case .push:
            viewController.navigationController?.pushViewController(featureViewController, animated: isAnimated)
        case .modal(let presentationStyle):
            featureViewController.modalPresentationStyle = presentationStyle
            viewController.present(featureViewController, animated: true)
        }
    }
}

extension Router {
    // MARK: - Private methods
    private static func getAssembledFeature(_ featureType: FeatureType) -> UIViewController {
        var featureViewController: UIViewController?
        switch featureType {
        case .details(let ad):
            featureViewController = DetailsViewController.assemble(with: ad)
        case .filterSettings(let delegate, let filters):
            featureViewController = FilterSettingsViewController.assemble(with: delegate, filters: filters)
        case .listItems:
            featureViewController = HomeViewController.assemble()
        case .search(let ads):
            featureViewController = SearchViewController.assemble(ads: ads)
        }
        
        guard let viewController = featureViewController else {
            fatalError("Feature error : feature has not been assembled")
        }
        return viewController
    }
}
