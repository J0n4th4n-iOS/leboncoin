//
//  AdCollectionViewCell.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation
import UIKit

protocol AdCollectionViewCellProtocol {
    func configure(with: Ad)
}

class AdCollectionViewCell: UICollectionViewCell {
    // MARK: Properties
    private let adImageView: UIImageView = {
        let image = UIImageView()
        image.clipsToBounds = true
        image.contentMode = .scaleAspectFill
        image.borderColor = .white
        image.borderWidth = Constants.BorderSize.adCell
        return image
    }()
    
    private let titleAdLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.title), numberOfLines: .zero)
    }()
    
    private let priceAdLabel: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.footNote))
    }()
    
    private let priorityAdImageView: UIImageView = {
        UIImageView(image: UIImage(named: "urgent"))
    }()
    
    // MARK: Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AdCollectionViewCell: AdCollectionViewCellProtocol {
    // MARK: AdCollectionViewCellProtocol
    func configure(with ad: Ad) {
        titleAdLabel.text = ad.title
        priceAdLabel.text = ad.price?.amountFormat()?.deviseSuffix
        adImageView.load(from: ad.images?.thumb, contentMode: .scaleAspectFill)
        configureUrgentMode(ad.isUrgent ?? false)
    }
}

extension AdCollectionViewCell {
    // MAR: Private methods
    private func configureUrgentMode(_ isUrgent: Bool) {
        priorityAdImageView.isHidden = !(isUrgent)
    }
    
    private func configureContentView()  {
        contentView.cornerRadius = Constants.Radius.backgroundCell
        contentView.borderColor = .whiteText
        contentView.borderWidth = Constants.BorderSize.adCell
        contentView.backgroundColor = .darkGray
    }
    
    private func buildUI() {
        // customize content view with tile effect
        configureContentView()
        
        // Ad image
        adImageView.anchor(in: contentView, top: contentView.topAnchor, leading: contentView.leadingAnchor, trailing: contentView.trailingAnchor)
        
        // Ad description
        let stackView = UIStackView(arrangedSubviews: [titleAdLabel, priceAdLabel])
        stackView.configure(alignment: .fill, spacing: Constants.Margin.minimumMargin)
   
        stackView.anchor(in: contentView,
                         top: adImageView.bottomAnchor, paddingTop: Constants.Margin.mediumMargin,
                         bottom: contentView.bottomAnchor, paddingBottom: Constants.Margin.largeMargin,
                         leading: contentView.leadingAnchor, paddingLeft: Constants.Margin.largeMargin,
                         trailing: contentView.trailingAnchor, paddingRight: Constants.Margin.largeMargin)
        
        // Urgent flag
        priorityAdImageView.anchor(in: contentView, top: contentView.topAnchor,
                                   leading: contentView.leadingAnchor, paddingLeft: Constants.Margin.largeMargin,
                                   width: Constants.Size.urgentImageViewWidth, height: Constants.Size.urgentImageViewHeight)
    }
}


