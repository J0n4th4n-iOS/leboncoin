//
//  AdCategoryCell.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation
import UIKit

protocol AdCategoryCellProtocol {
    func configure(for category: AdCategory, with ads: [Ad], parentController: HomeViewCollectionDelegate?)
}

class AdCategoryTableViewCell: UITableViewCell {
    // MARK: Properties
    let headerTitle: UILabel = {
        UILabel(with: .boldSystemFont(ofSize: Constants.FontSize.sectionCell))
    }()
    
    var collectionView: CollectionViewProtocol?
    
    // MARK: Init
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension AdCategoryTableViewCell: AdCategoryCellProtocol {
    // MARK: AdCategoryCellProtocol
    func configure(for category: AdCategory, with ads: [Ad], parentController: HomeViewCollectionDelegate?) {
        headerTitle.text = category.title
        configureChildViews(in: parentController, ads: ads)
    }
}

extension AdCategoryTableViewCell {
    // MARK: Private methods
    private func buildUI() {
        backgroundColor = .darkGray

        headerTitle.anchor(in: contentView,
                           top: contentView.topAnchor, paddingTop: Constants.Margin.mediumMargin,
                           leading: contentView.leadingAnchor, paddingLeft: Constants.Margin.mediumMargin,
                           trailing: contentView.trailingAnchor, paddingRight: Constants.Margin.mediumMargin)
    }
    
    private func configureChildViews(in parent: HomeViewCollectionDelegate?, ads: [Ad]) {
        //Si collectionView child already define then just refresh datas
        if let collectionView = collectionView {
            collectionView.refresh(with: ads)
        } else {
            //Otherwise init collectionView
            let collectionViewController =  CollectionViewController.assemble(with: ads)
            collectionView = collectionViewController
            parent?.addCategoryChild(collectionViewController)

            collectionViewController.view.anchor(in: contentView,
                                                 top: headerTitle.bottomAnchor, paddingTop: Constants.Margin.mediumMargin,
                                                 bottom: contentView.bottomAnchor,
                                                 leading: contentView.leadingAnchor,
                                                 trailing: contentView.trailingAnchor)
        }
    }
}
