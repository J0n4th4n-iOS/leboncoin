//
//  CollectionPresenter.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation

class CollectionPresenter {
    // MARK: Properties
    weak var view: CollectionViewProtocol?
    var ads : [Ad]?
    
    // MARK: Init
    init(with view: CollectionViewProtocol?, ads: [Ad]) {
        self.view = view
        self.ads = ads
    }
}

extension CollectionPresenter: CollectionPresenterProtocol {
    // MARK: CollectionPresenterProtocol
    func configure(_ cell: AdCollectionViewCellProtocol, atIndexPath indexPath: IndexPath) {
        guard let ad = ads?.getOrNull(indexPath.row) else { return }
        cell.configure(with: ad)
    }
    
    func numberOfItems() -> Int {
        ads?.count ?? 0
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        guard let ad = ads?.getOrNull(indexPath.row) else { return }
        view?.showDetails(for: ad)
    }
    
    func refresh(with ads: [Ad]) {
        self.ads = ads
        view?.reloadData()
    }
}
