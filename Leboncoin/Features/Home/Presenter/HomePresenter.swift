//
//  HomePresenter.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

class HomePresenter {
    // MARK: Properties
    private weak var view: (HomeViewProtocol & HomeViewCollectionDelegate)?
    private var manager: HomeManagerProtocol?
    var dataSourceManager: HomeDataSourceManagerProtocol?
    
    // MARK: Init
    public init(with view: (HomeViewProtocol & HomeViewCollectionDelegate)?, manager: HomeManagerProtocol?) {
        self.view = view
        self.manager = manager
        self.dataSourceManager = HomeDataSourceManager(with: self, ads: [])
    }
}

extension HomePresenter: HomePresenterProtocol {
    // MARK: HomePresenterProtocol
    var filters: [AdCategory] {
        dataSourceManager?.filters ?? []
    }
    
    func didLoad() {
        manager?.loadItems(success: { [weak self] ads in
            guard let self = self else { return }
            
            self.view?.buildSearch()
            self.dataSourceManager?.sortAdsByCategory(ads)
            self.view?.reloadData(forState: .loaded)
        }, failure: { error in
            self.view?.reloadData(forState: .error)
        })
    }
    
    func configure(_ cell: AdCategoryCellProtocol, atIndexPath indexPath: IndexPath) {
        guard
            let category = dataSourceManager?.getCategory(at: indexPath.row),
            let ads = dataSourceManager?.getAds(for: category)
        else {
            return
        }
    
        cell.configure(for: category, with: ads, parentController: view)
    }
    
    func numberOfRows(atSection section: Int) -> Int {
        dataSourceManager?.getCategories()?.count ?? .zero
    }
    
    func getFilters() -> [AdCategory] {
        dataSourceManager?.filters ?? []
    }
    
    func applyFilters(_ adCategories: [AdCategory]) {
        dataSourceManager?.updateFiltersAds(with: adCategories)
        view?.reloadData(forState: dataSourceManager?.filteredAds?.isEmpty ?? true ? .empty : .loaded)
    }
    
    func getAds() -> [Ad] {
        dataSourceManager?.getAdsSorted() ?? []
    }
}
