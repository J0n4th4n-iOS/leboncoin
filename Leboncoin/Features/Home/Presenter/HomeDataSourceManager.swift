//
//  HomeDataSourceManager.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

class HomeDataSourceManager {
    // MARK: Properties
    weak var presenter: HomePresenterProtocol?
    
    var filteredAds: [AdCategory: [Ad]]?
    var adsByCategory: [AdCategory: [Ad]]? = [:] {
        didSet {
            filteredAds = adsByCategory
        }
    }
    var filters: [AdCategory] = []
    
    // MARK: Init
    init(with presenter: HomePresenterProtocol?, ads: [Ad]?) {
        self.presenter = presenter
    }
}

extension HomeDataSourceManager: HomeDataSourceManagerProtocol {
    // MARK: HomeDataSourceManagerProtocol
    func sortAdsByCategory(_ ads: [Ad]?) {
        AdCategory.allCases.forEach { [weak self] category in
            //get ads for a specific category
            let adsCategory = category == .spotlight ? ads?.filter { $0.isUrgent ?? false} : ads?.filter { $0.categoryId == category }
            //Store ads by category
            self?.adsByCategory?[category] = sortAds(adsCategory)
        }
    }
    
    func getCategory(at index: Int) -> AdCategory? {
        let orderedcategories = getCategories()?.sorted { $0.position < $1.position }
        return orderedcategories?.getOrNull(index)
    }
    
    func getCategories() -> [AdCategory]? {
        guard let adsByCategory = filteredAds else { return nil }
        
        return Array(adsByCategory.keys)
    }
    
    func getAds(for category: AdCategory) -> [Ad]? {
        filteredAds?[category]
    }
    
    func getAdsSorted() -> [Ad]? {
        var tmpAdsByCategory = adsByCategory
        tmpAdsByCategory?.removeValue(forKey: .spotlight)
        let ads = tmpAdsByCategory.map { $0.values.flatMap { $0 }}
        
        return sortAds(ads)
    }
    
    func updateFiltersAds(with filters: [AdCategory]) {
        self.filters = filters
        if !filters.isEmpty {
            filteredAds?.removeAll()
            filters.forEach { filteredAds?[$0] = adsByCategory?[$0] }
        } else {
            filteredAds = adsByCategory
        }
    }
}

extension HomeDataSourceManager {
    // MARK: Private methods
    private func sortAds(_ ads: [Ad]?) -> [Ad] {
        guard let ads = ads else { return [] }
        
        return ads.sorted { Date.dateFormatter(date: $0.creationDate ?? "") > Date.dateFormatter(date: $1.creationDate ?? "") }
    }
}
