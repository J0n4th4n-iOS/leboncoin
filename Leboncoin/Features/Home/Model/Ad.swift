//
//  Ads.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

enum AdCategory: Int64, Codable, CaseIterable {
    case vehicle = 1, fashion = 2, doItYourself = 3, house = 4, hobbies = 5, immovable = 6, booksCdDvd = 7, multimedia = 8, service = 9, animals = 10, children = 11, spotlight = 12
    
    var title: String {
        switch self {
        case .vehicle: return "vehicle".localized
        case .fashion: return "fashion".localized
        case .doItYourself: return "doItYourself".localized
        case .house: return "house".localized
        case .hobbies: return "hobbies".localized
        case .immovable: return "immovable".localized
        case .booksCdDvd: return "booksCdDvd".localized
        case .multimedia: return "multimedia".localized
        case .service: return "service".localized
        case .animals: return "animals".localized
        case .children: return "children".localized
        case .spotlight: return "spotlight".localized
        }
    }
    
    var position: Int {
        switch self {
        case .spotlight: return 0
        case .vehicle: return 1
        case .immovable: return 2
        case .house: return 3
        case .multimedia: return 4
        case .booksCdDvd: return 5
        case .animals: return 6
        case .fashion: return 7
        case .hobbies: return 8
        case .doItYourself: return 9
        case .service: return 10
        case .children: return 11
        }
    }
    
    static func getCategory(for section: Int) -> AdCategory? {
        AdCategory.allCases.first { $0.position == section }
    }
}

class ImageAd: Codable {
    var small: String?
    var thumb: String?
}

class Ad: Codable {
    var id: Int64?
    var categoryId: AdCategory?
    var title: String?
    var description: String?
    var price: Float?
    var images: ImageAd?
    var creationDate: String?
    var isUrgent: Bool?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case categoryId = "category_id"
        case title
        case description
        case price
        case images = "images_url"
        case creationDate = "creation_date"
        case isUrgent = "is_urgent"
    }
}
