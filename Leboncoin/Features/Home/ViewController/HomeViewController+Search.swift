//
//  HomeViewController+Search.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 07/09/2021.
//

import Foundation
import UIKit

extension HomeViewController {
    // MARK: Build search
    func buildSearchComponent() {
        searchBar.delegate = self
        searchBar.placeholder = "searchAds".localized
        
        navigationItem.titleView = searchBar
    }
}

extension HomeViewController: UISearchBarDelegate {
    // MARK: UISearchBarDelegate
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        Router.showFeature(.search(ads: presenter?.getAds() ?? []), from: self,
                           presentMode: .push)
        return false
    }
}
