//
//  HomeViewController+iPhone.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation
import UIKit

extension HomeViewController {
    // MARK: Private methods
    func configureTableView() {
        tableView?.separatorStyle = .none
        tableView?.backgroundColor = .grayApp
        tableView?.allowsSelection = false
        tableView?.register(AdCategoryTableViewCell.self,
                            forCellReuseIdentifier: ReuseIdentifier.adTableViewCellIdentifier.rawValue)
        refreshTableView(forState: .loading)
    }
    
    func refreshTableView(forState state: DataState) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            state == .loaded ? self.tableView.restore() :
                self.tableView?.setBackgroundView(with: state.message, image: state.image)
            self.tableView.reloadData()
        }
        
    }
}

extension HomeViewController {
    // MARK: UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter?.numberOfRows(atSection: section) ?? .zero
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard
            let cell = tableView.dequeueReusableCell(withIdentifier: ReuseIdentifier.adTableViewCellIdentifier.rawValue, for: indexPath) as? AdCategoryTableViewCell
        else {
            return UITableViewCell()
        }
        
        presenter?.configure(cell, atIndexPath: indexPath)
        return cell
    }
}

extension HomeViewController {
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.Size.rowHeight
    }
}

