//
//  CollectionViewController.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation
import UIKit

class CollectionViewController: UICollectionViewController {
    // MARK: Properties
    var presenter: CollectionPresenterProtocol?
    
    // MARK: Assemble
    static func assemble(with ads: [Ad]) ->  CollectionViewController {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        
        let collectionViewController = CollectionViewController(collectionViewLayout: flowLayout)
        collectionViewController.presenter = CollectionPresenter(with: collectionViewController, ads: ads)
        return collectionViewController
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
    }
}

extension CollectionViewController: UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDataSource
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        presenter?.numberOfItems() ?? .zero
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseIdentifier.adCollectionViewCellIdentifier.rawValue, for: indexPath) as? AdCollectionViewCell else { return UICollectionViewCell() }
        
        presenter?.configure(cell, atIndexPath: indexPath)
        return cell
    }
    
    // MARK: UICollectionViewDelegate
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.didSelectItem(at: indexPath)
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: Constants.Size.adCellWidth, height: view.frame.height - Constants.Size.adCellHeightPading)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: .zero,
                     left: Constants.EdgeInset.collectionViewSection,
                     bottom: .zero,
                     right: Constants.EdgeInset.collectionViewSection)
    }
}

extension CollectionViewController: CollectionViewProtocol {
    // MARK: CollectionViewProtocol
    func reloadData() {
        collectionView.reloadData()
    }
    
    func refresh(with ads: [Ad]) {
        presenter?.refresh(with: ads)
    }
    
    func showDetails(for ad: Ad) {
        Router.showFeature(.details(ad: ad), from: self)
    }
}

extension CollectionViewController {
    // MARK: Private methods
    private func setupCollectionView() {
        collectionView.backgroundColor = .grayApp
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.backgroundColor = .grayApp
        collectionView?.register(AdCollectionViewCell.self,
                                 forCellWithReuseIdentifier: ReuseIdentifier.adCollectionViewCellIdentifier.rawValue)
    }
}
