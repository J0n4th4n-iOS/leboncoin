//
//  ViewController.swift
//  leboncoin
//
//  Created by Jonathan BACQUEY on 03/09/2021.
//

import UIKit

class HomeViewController: UITableViewController {
    // MARK: properties
    var presenter: HomePresenterProtocol?
    lazy var searchBar = UISearchBar(frame: .zero)
    
    // MARK: Assemble
    static func assemble() -> HomeViewController {
        let homeViewController = HomeViewController()
        homeViewController.presenter = HomePresenter(with: homeViewController, manager: HomeManager())
        return homeViewController
    }
    
    // MARK: View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        buildUI()
        presenter?.didLoad()
    }
}

extension HomeViewController: HomeViewProtocol {
    // MARK: HomeViewProtocol
    func buildSearch() {
        addNavigationBarButtonItem()
        buildSearchComponent()
    }
    
    func reloadData(forState state: DataState) {
        refreshTableView(forState: state)
    }
    
    
}

extension HomeViewController: HomeViewSettingsDelegate {
    // MARK: HomeViewSettingsDelegate
    func applyFilters(_ adCategories: [AdCategory]) {
        updateFilterButton(isFiltering: !adCategories.isEmpty)
        presenter?.applyFilters(adCategories)
    }
}

extension HomeViewController: HomeViewCollectionDelegate {
    //MARK: HomeViewCollectionDelegate
    func addCategoryChild(_ childController: UIViewController) {
        addChild(childController)
        childController.didMove(toParent: self)
    }
}

extension HomeViewController {
    // MARK: Private methods
    private func buildUI() {
        title = "homeTitle".localized
        view.backgroundColor = .grayApp
        configureTableView()
    }
    
    private func addNavigationBarButtonItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.customBarButtonItem(self,
                                                                   action: #selector(filterSettings),
                                                                   imageName: "filter",
                                                                   tintColor: .white)
    }
    
    private func updateFilterButton(isFiltering: Bool) {
        navigationItem.rightBarButtonItem?.customView?.tintColor = isFiltering ? .red : .white
    }
    
    @objc private func filterSettings() {
        Router.showFeature(.filterSettings(delegate: self, filtersEnabled: presenter?.filters ?? []), from: self, isAnimated: false)
    }
}
