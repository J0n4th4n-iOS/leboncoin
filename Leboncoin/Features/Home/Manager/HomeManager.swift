//
//  HomeManager.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

class HomeManager: HomeManagerProtocol {
    // MARK: Properties
    lazy var serviceManager: ServiceManagerProtocol = ServiceManager.shared
    lazy var decoder = Decoder<[Ad]>()
    
    // MARK: HomeManagerProtocol
    func loadItems(success: @escaping ([Ad]) -> Void, failure: @escaping (Error) -> Void) {
        serviceManager.request(.loadItems) { [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success(let adsData):
                if let items = self.decoder.decode(with: adsData) {
                    success(items)
                } else {
                    failure(ServiceError.missingData)
                }
            case .failure(let error):
                failure(error)
            }
        }
    }
}
