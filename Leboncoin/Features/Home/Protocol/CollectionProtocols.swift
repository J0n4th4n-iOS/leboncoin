//
//  CollectionProtocols.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation

protocol CollectionViewProtocol: class {
    // MARK: CollectionViewProtocol
    func reloadData()
    func refresh(with ads: [Ad])
    func showDetails(for ad: Ad)
}

protocol CollectionPresenterProtocol {
    // MARK: CollectionPresenterProtocol
    func numberOfItems() -> Int
    func configure(_ cell: AdCollectionViewCellProtocol, atIndexPath indexPath: IndexPath)
    func didSelectItem(at indexPath: IndexPath)
    func refresh(with ads: [Ad])
}
