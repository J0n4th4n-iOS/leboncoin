//
//  HomeProtocols.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation
import UIKit

protocol HomeViewProtocol: class {
    // MARK: - HomeViewProtocol
    func buildSearch()
    func reloadData(forState state: DataState)
}

protocol HomeViewSettingsDelegate: class {
    func applyFilters(_ adCategories: [AdCategory])
}

protocol HomeViewCollectionDelegate: class {
    func addCategoryChild(_ childViewController: UIViewController)
}

protocol HomePresenterProtocol: class {
    // MARK: - HomePresenterProtocol
    var filters: [AdCategory] { get }
    func didLoad()
    func numberOfRows(atSection section: Int) -> Int
    func configure(_ cell: AdCategoryCellProtocol, atIndexPath indexPath: IndexPath)
    func getAds() -> [Ad]
    func applyFilters(_ adCategories: [AdCategory])
}

protocol HomeDataSourceManagerProtocol {
    var filters: [AdCategory] { get set }
    var filteredAds: [AdCategory: [Ad]]? { get }
    
    func sortAdsByCategory(_ ads: [Ad]?)
    func getCategory(at index: Int) -> AdCategory?
    func getCategories() -> [AdCategory]?
    func getAds(for category: AdCategory) -> [Ad]?
    func getAdsSorted() -> [Ad]?
    func updateFiltersAds(with filters: [AdCategory])
}

protocol HomeManagerProtocol {
    // MARK: - HomeManagerProtocol
    func loadItems(success: @escaping ([Ad]) -> Void, failure: @escaping (Error) -> Void)
}
