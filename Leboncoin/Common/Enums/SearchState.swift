//
//  SearchState.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

enum SearchState {
    case editing
    case empty
    case found
    
    var message: String {
        switch self {
        case .editing:
            return "editing".localized
        case .empty:
            return "empty".localized
        case .found:
            return "found".localized
        }
    }
    
    var image: UIImage? {
        switch self {
        case .empty, .editing :
            return UIImage(named: "information")
        case .found:
            return nil
        }
    }
}
