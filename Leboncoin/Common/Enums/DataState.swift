//
//  DataState.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

enum DataState {
    case empty, error, loaded, loading
    
    var image: UIImage? {
        switch self {
        case .empty, .loading :
            return UIImage(named: "information")
        case .error :
            return UIImage(named: "error")
        case .loaded:
            return nil
        }
    }
    
    var message: String {
        switch self {
        case .empty:
            return "emptyMessage".localized
        case .error:
            return "errorMessage".localized
        case .loaded:
            return ""
        case .loading:
            return "loadingMessage".localized
        }
    }
}
