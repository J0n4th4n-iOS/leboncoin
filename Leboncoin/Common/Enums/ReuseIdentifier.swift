//
//  ReuseIdentifier.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

enum ReuseIdentifier: String {
    case adTableViewCellIdentifier = "AdTableViewCellIdentifier"
    case adCollectionViewCellIdentifier = "AdCollectionViewCellIdentifier"
    case adCategoryCellIdentifier = "AdCategoryCellIdentifier"
    case searchCellIdentifier = "SearchCellIdentifier"
}
