//
//  UITableView+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

extension UITableView {
    func setBackgroundView(with message: String?, image: UIImage?) {
        showBackgroundView(with: message, image: image)
    }
    
    func restore() {
        self.backgroundView = nil
    }
}

extension UITableView {
    private func showBackgroundView(with message: String?, image: UIImage?) {
        let messageLabel = UILabel(with: .systemFont(ofSize: Constants.FontSize.backgroundViewMessage),
                                   textColor: .blueApp,
                                   textAlignment: .center,
                                   numberOfLines: .zero)
        messageLabel.text = message

        let messageStackView = UIStackView(arrangedSubviews: [messageLabel])
        messageStackView.configure(with: .horizontal, alignment: .bottom, distribution: .fill)
        
        let imageView = UIImageView(image: image)

        let imageStackView = UIStackView(arrangedSubviews: [imageView])
        imageStackView.configure(with: .horizontal, alignment: .top, distribution: .fill)
        
        let stackView = UIStackView(arrangedSubviews: [messageStackView, imageStackView])
        stackView.configure(alignment: .center, distribution: .fillEqually)
        stackView.customize(with: .grayApp)
        
        backgroundView = stackView
    }
}
