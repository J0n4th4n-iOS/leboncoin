//
//  UIColor+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

extension UIColor {
    static let grayApp = UIColor(red: 44.0/255.0, green: 60.0/255.0, blue: 83.0/255.0, alpha: 1.0)
    static let blueApp = UIColor(red: 17.0/255.0, green: 119.0/255.0, blue: 166.0/255.0, alpha: 1.0)
    static let backgroundView = UIColor(red: 140.0/255.0, green: 200.0/255.0, blue: 219.0/255.0, alpha: 1.0)
    static let whiteText = UIColor(white: 1, alpha: 0.8)
}
