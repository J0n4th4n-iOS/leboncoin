//
//  UIStackView+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

extension UIStackView {
    func configure(with axis: NSLayoutConstraint.Axis = .vertical,
                   alignment: Alignment = .fill,
                   distribution: Distribution = .fillProportionally,
                   spacing: CGFloat = .zero) {
        self.axis = axis
        self.alignment = alignment
        self.distribution = distribution
        self.spacing = spacing
        
    }
}
