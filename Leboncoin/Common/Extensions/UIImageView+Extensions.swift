//
//  UIImageView+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 07/09/2021.
//

import Foundation
import UIKit

extension UIImageView {
    var imageLoader: ImageLoaderProtocol {
        return ImageLoader.shared
    }
    
    func load(from link: String?,
                  contentMode mode: ContentMode = .scaleAspectFit,
                  defaultImageName: String = "emptyImage",
                  completionHandler: (() -> Void)? = nil) {
        guard let link = link, let url = URL(string: link) else { return }
        
        contentMode = mode
        // Set default image
        image = UIImage(named: defaultImageName)
        ImageLoader.shared.loadImage(from: url) { [weak self] image in
            if let image = image {
                DispatchQueue.main.async {
                    self?.image = image
                }
            }
        }
    }
}
