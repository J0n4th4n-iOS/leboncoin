//
//  UINavigationController.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 13/09/2021.
//

import Foundation
import UIKit

extension UINavigationController {
    func clearBackButtonTitle() {
        navigationBar.topItem?.title = ""
    }
}
