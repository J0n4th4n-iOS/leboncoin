//
//  Float+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 11/09/2021.
//

import Foundation

extension Float {
    private static let amountFormatter: NumberFormatter = staticFormatter()
    
    static func staticFormatter() -> NumberFormatter {
        let numberFormatter = NumberFormatter()
        let locale = Locale(identifier: "fr_FR")
        numberFormatter.locale = locale
        numberFormatter.usesGroupingSeparator = false
        numberFormatter.groupingSeparator = locale.groupingSeparator
        return numberFormatter
    }
    
    func amountFormat(minimumFractionDigits: Int = 0, maximumFractionDigits: Int = 2) -> String? {
        Float.amountFormatter.numberStyle = .decimal
        Float.amountFormatter.minimumFractionDigits = minimumFractionDigits
        Float.amountFormatter.maximumFractionDigits = maximumFractionDigits
        return Float.amountFormatter.string(for: self)
    }
}
