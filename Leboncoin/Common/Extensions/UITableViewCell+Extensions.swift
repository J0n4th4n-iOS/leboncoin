//
//  UITableViewCell+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation
import UIKit

extension UITableViewCell {
    func configure(with selectionStyle: UITableViewCell.SelectionStyle? = nil, textColor: UIColor? = nil, font: UIFont? = nil, backgroundColor: UIColor? = nil, text: String? = nil ) {
        self.textLabel?.text = text
        
        if let selectionStyle = selectionStyle {
            self.selectionStyle = selectionStyle
        }
        if let textColor = textColor {
            self.textLabel?.textColor = textColor
        }
        if let font = font {
            self.textLabel?.font = font
        }
        if let backgroundColor = backgroundColor {
            self.backgroundColor = backgroundColor
        }
    }
}
