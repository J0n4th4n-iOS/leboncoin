//
//  Date+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 09/09/2021.
//

import Foundation

enum DateFormat: String {
    case defaultTimeZone = "yyyy-MM-dd'T'HH:mm:ssZ"
    case dayAndMonthWithHours = "dd MMMM yyyy à HH'h'mm"
}

extension Date {
    static let dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "fr_FR")
        return dateFormatter
    }()
    
    static func dateFormatter(with format: DateFormat = .defaultTimeZone, date: String) -> Date {
        Date.dateFormatter.dateFormat = format.rawValue
        return Date.dateFormatter.date(from: date) ?? Date()
    }
    
    func toString(format: DateFormat = .defaultTimeZone) -> String {
        Date.dateFormatter.dateFormat = format.rawValue
        return Date.dateFormatter.string(from: self)
    }
}


