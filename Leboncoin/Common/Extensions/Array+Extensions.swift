//
//  Array+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

extension Array {
    func getOrNull(_ index: Int) -> Element? {
        guard index >= .zero, index < endIndex else {
            return nil
        }
        return self[index]
    }
}

extension RangeReplaceableCollection where Element: Equatable {
    mutating func shouldAddOnSelection(_ element: Element) -> Bool {
        // Add the object if it is not present, otherwise delete it
        // return true if added otherwise false
        if let index = self.firstIndex(of: element) {
            self.remove(at: index)
            return false
        }
        else {
            self.append(element)
            return true
        }
    }
}

