//
//  UILabel+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 11/09/2021.
//

import Foundation
import UIKit

extension UILabel {
    convenience init(with font: UIFont, textColor: UIColor = .whiteText, textAlignment: NSTextAlignment = .left, numberOfLines: Int = 1) {
        self.init()
    
        self.font = font
        self.textColor = textColor
        self.textAlignment = textAlignment
        self.numberOfLines = numberOfLines
    }
    
}
