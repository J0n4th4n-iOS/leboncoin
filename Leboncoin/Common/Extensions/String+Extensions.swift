//
//  String+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 07/09/2021.
//

import Foundation

extension String {
    var localized: String {
        NSLocalizedString(self, tableName: "Strings", comment: "")
    }
    
    var deviseSuffix: String {
        self + "€"
    }
    
    func prefix(with string: String) -> String {
        string + self
    }
    
    var neutral: String {
        folding(options: [.diacriticInsensitive, .caseInsensitive], locale: .current)
    }
    
    func toDate(inputFormat: DateFormat = .defaultTimeZone, outputFormat: DateFormat = .defaultTimeZone) -> String? {
        let datetmp = Date.dateFormatter(with: inputFormat, date: self)
        return datetmp.toString(format: outputFormat)
    }
}
