//
//  UIView+Extensions.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation
import UIKit

typealias SafeAreaLayoutGuideEnabled = (top: Bool, leading: Bool, trailing: Bool, bottom: Bool)

extension UIView {
    func showIn(view: UIView,
                withSpacing spacing: CGFloat = .zero,
                safeAreaLayoutGuideEnabled: SafeAreaLayoutGuideEnabled = (false, false, false, false)) {
        view.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        
        topAnchor.constraint(equalTo: safeAreaLayoutGuideEnabled.top ?
                                view.safeAreaLayoutGuide.topAnchor : view.topAnchor,
                             constant: spacing).isActive = true
        bottomAnchor.constraint(equalTo: safeAreaLayoutGuideEnabled.bottom ?
                                    view.safeAreaLayoutGuide.bottomAnchor : view.bottomAnchor,
                                constant: -spacing).isActive = true
        leadingAnchor.constraint(equalTo: safeAreaLayoutGuideEnabled.leading ?
                              view.safeAreaLayoutGuide.leadingAnchor : view.leadingAnchor,
                              constant: spacing).isActive = true
        trailingAnchor.constraint(equalTo: safeAreaLayoutGuideEnabled.trailing ?
                                view.safeAreaLayoutGuide.trailingAnchor : view.trailingAnchor,
                               constant: -spacing).isActive = true
    }
    
    func anchor(in view: UIView,
                top: NSLayoutYAxisAnchor? = nil, paddingTop: CGFloat = 0,
                bottom: NSLayoutYAxisAnchor? = nil, paddingBottom: CGFloat = 0,
                leading: NSLayoutXAxisAnchor? = nil, paddingLeft: CGFloat = 0,
                trailing: NSLayoutXAxisAnchor? = nil, paddingRight: CGFloat = 0,
                width: CGFloat = 0, height: CGFloat = 0) {
        view.addSubview(self)
        translatesAutoresizingMaskIntoConstraints = false
        
        if let top = top {
            topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
        }
        if let bottom = bottom {
            bottomAnchor.constraint(equalTo: bottom, constant: -paddingBottom).isActive = true
        }
        if let trailing = trailing {
            trailingAnchor.constraint(equalTo: trailing, constant: -paddingRight).isActive = true
        }
        if let leading = leading {
            leadingAnchor.constraint(equalTo: leading, constant: paddingLeft).isActive = true
        }
        if width != 0 {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        if height != 0 {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > .zero
        }
    }
    
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    var borderColor: UIColor? {
        get {
            guard let borderColor = layer.borderColor else { return nil }
            return UIColor(cgColor: borderColor)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func customize(with backgroundColor: UIColor? = nil,
                   borderColor: UIColor? = nil,
                   borderWidth: CGFloat? = nil,
                   cornerRadius: CGFloat? = nil) {
        if let backgroundColor = backgroundColor {
            self.backgroundColor = backgroundColor
        }
        
        if let borderColor = borderColor {
            self.borderColor = borderColor
        }
        
        if let borderWidth = borderWidth {
            self.borderWidth = borderWidth
        }
        
        if let cornerRadius = cornerRadius {
            self.cornerRadius = cornerRadius
        }
    }
}
