//
//  ServiceManager.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 04/09/2021.
//

import Foundation

protocol ServiceManagerProtocol {
    func request(_ endpoint: Endpoint, completion: @escaping (Result) -> Void)
}

enum Endpoint: String {
    case loadItems = "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json"
}

enum ServiceError: Error {
    case missingData, wrongEndpoint
}

 enum Result {
    case failure(Error), success(Data)
}

class ServiceManager {
    static let shared = ServiceManager()
    let defaultSession = URLSession(configuration: .default)
}

extension ServiceManager: ServiceManagerProtocol {
    // MARK: ServiceManagerProtocol
    func request(_ endpoint: Endpoint, completion: @escaping (Result) -> Void) {
        guard let url = URL(string: endpoint.rawValue) else {
            completion(.failure(ServiceError.wrongEndpoint))
            return
        }
        
        let dataTask = defaultSession.dataTask(with: url) { data, _, error in
            if let error = error { completion(.failure(error)) }
            guard let data = data, !data.isEmpty else {
                completion(.failure(ServiceError.missingData))
                return
            }
            
            DispatchQueue.main.async {
                completion(.success(data))
            }
        }
        dataTask.resume()
    }
}
