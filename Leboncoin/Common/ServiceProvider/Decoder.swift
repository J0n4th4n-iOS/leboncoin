//
//  Decoder.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation

struct Decoder<T> where T: Decodable {
    func decode(with data: Data?) -> T? {
        guard let data = data else { return nil }
        
        do {
            let object = try JSONDecoder().decode(T.self, from: data)
            return object
        } catch {
            return nil
        }
    }
}
