//
//  Constants.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import CoreGraphics

struct Constants {
    struct Margin {
        static let minimumMargin: CGFloat = 4.0
        static let mediumMargin: CGFloat = 8.0
        static let largeMargin: CGFloat = 16.0
    }
    
    struct EdgeInset {
        static let capsuleView: CGFloat = 5.0
        static let collectionViewSection: CGFloat = 10.0
    }
    
    struct FontSize {
        static let sectionCell: CGFloat = 18.0
        static let title: CGFloat = 16.0
        static let body: CGFloat = 14.0
        static let footNote: CGFloat = 12.0
        static let backgroundViewMessage: CGFloat = 16.0
    }
    
    struct Radius {
        static let backgroundCell: CGFloat = 10.0
        static let searchCell: CGFloat = 20.0
        static let capsuleView: CGFloat = 4.0
    }
    
    struct Size {
        static let imageTableViewCellHeight: CGFloat = 150.0
        static let urgentImageViewWidth: CGFloat = 25.0
        static let urgentImageViewHeight: CGFloat = 35.0
        static let adCellWidth: CGFloat = 220.0
        static let adCellHeightPading: CGFloat = 60.0
        static let rowHeight: CGFloat = 400.0
        static let separator: CGFloat = 2.0
        static let estimatedHeightCell: CGFloat = 44.0
        static let halfRatio: CGFloat = 0.5
    }
    
    struct BorderSize {
        static let capsuleView: CGFloat = 1.0
        static let adCell: CGFloat = 2.0
    }
}
