//
//  ImageCacheProtocol.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

protocol ImageCacheProtocol: class {
    // Returns the image associated with a given url
    func image(for url: URL) -> UIImage?
    // Inserts the image of the specified url in the caches
    func insertImage(_ image: UIImage?, for url: URL)
    // Removes the image of the specified url in the caches
    func removeImage(for url: URL)
    // Removes all images from the caches
    func removeAllImages()
    // Accesses the value associated with the given key for reading and writing
    subscript(_ url: URL) -> UIImage? { get set }
}
