//
//  ImageCache.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

final class ImageCache {

    // MARK: Properties
    private lazy var decodedImageCache: NSCache<AnyObject, AnyObject> = {
        let cache = NSCache<AnyObject, AnyObject>()
        cache.totalCostLimit = config.memoryLimit
        return cache
    }()
    
    private let lock = NSLock()
    private let config: Config

    struct Config {
        let memoryLimit: Int
        // 100 MB maximum for cache system
        static let defaultConfig = Config(memoryLimit: 1024 * 1024 * 100)
    }

    // MARK: Init
    init(config: Config = Config.defaultConfig) {
        self.config = config
    }
}

extension ImageCache: ImageCacheProtocol {
    // MARK: ImageCacheProtocol
    func image(for url: URL) -> UIImage? {
        lock.lock(); defer { lock.unlock() }
        // the best case scenario -> there is a decoded image
        if let decodedImage = decodedImageCache.object(forKey: url as AnyObject) as? UIImage {
            return decodedImage
        }
        
        return nil
    }
    
    func insertImage(_ image: UIImage?, for url: URL) {
        guard let image = image else { return removeImage(for: url) }
        let decodedImage = image.decodedImage()

        lock.lock(); defer { lock.unlock() }
        decodedImageCache.setObject(decodedImage, forKey: url as AnyObject, cost: decodedImage.pngData()?.count ?? .zero)
    }
    
    func removeImage(for url: URL) {
        lock.lock(); defer { lock.unlock() }
        decodedImageCache.removeObject(forKey: url as AnyObject)
    }
    
    func removeAllImages() {
        lock.lock(); defer { lock.unlock() }
        decodedImageCache.removeAllObjects()
    }
    
    subscript(_ key: URL) -> UIImage? {
        get {
            return image(for: key)
        }
        set {
            return insertImage(newValue, for: key)
        }
    }
}
