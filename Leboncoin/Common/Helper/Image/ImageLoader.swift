//
//  ImageLoader.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

protocol ImageLoaderProtocol {
    func loadImage(from url: URL, completionHandler: @escaping ((UIImage?) -> Void))
}

final class ImageLoader {
    static let shared = ImageLoader()
    private var imageCache: ImageCacheProtocol = ImageCache()
}

extension ImageLoader: ImageLoaderProtocol {
    func loadImage(from url: URL, completionHandler: @escaping ((UIImage?) -> Void)) {
        // first check in cache
        if let image = imageCache[url] {
            completionHandler(image)
        } else {
            // Else download image and store in cache
            download(from: url) { [weak self] image in
                guard let image = image else {
                    completionHandler(nil)
                    return
                }
                self?.imageCache[url] = image
                completionHandler(image)
            }
        }
    }
}

extension ImageLoader {
    // MARK: Private methods
    private func download(from url: URL, completionHandler: ((UIImage?) -> Void)? = nil) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                completionHandler?(UIImage(data: data))
            } else {
                completionHandler?(nil)
            }
        }
    }
}
