//
//  CapsuleView.swift
//  Leboncoin
//
//  Created by Jonathan BACQUEY on 08/09/2021.
//

import Foundation
import UIKit

class CapsuleView: UILabel {
    private var edgeInsets = UIEdgeInsets(top: Constants.EdgeInset.capsuleView,
                                          left: Constants.EdgeInset.capsuleView,
                                          bottom: Constants.EdgeInset.capsuleView,
                                          right: Constants.EdgeInset.capsuleView)
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: edgeInsets))
    }

    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + edgeInsets.left + edgeInsets.right,
                      height: size.height + edgeInsets.top + edgeInsets.bottom)
    }
    
    func setTextColor(_ color: UIColor) {
        textColor = color
    }
    
    func setBorderColor(_ color: UIColor) {
        borderColor = color
    }
    
    func setColor(_ color: UIColor) {
        setTextColor(color)
        setBorderColor(color)
    }
}

extension CapsuleView {
    // MARK: Private methods
    private func setupView() {
        cornerRadius = Constants.Radius.capsuleView
        borderWidth =  Constants.BorderSize.capsuleView
        setTextColor(.red)
        setBorderColor(.red)
        font = UIFont.boldSystemFont(ofSize: Constants.FontSize.footNote)
    }
}
