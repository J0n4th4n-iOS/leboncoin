//
//  AppDelegate.swift
//  leboncoin
//
//  Created by Jonathan BACQUEY on 03/09/2021.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - Properties
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        Router.showFeatureAsRoot(.listItems, isNavigationControllerRequired: true, on: window)
        customizeNavigationAppearence()
        window?.makeKeyAndVisible()
        
        return true
    }
    
    private func customizeNavigationAppearence() {
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UINavigationBar.appearance().barTintColor = .grayApp
        UINavigationBar.appearance().tintColor = .whiteText
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
    }
}

