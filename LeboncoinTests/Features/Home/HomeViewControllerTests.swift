//
//  HomeViewControllerTests.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import XCTest

class HomeViewControllerTests: XCTestCase {
    private typealias Package = (sut: HomeViewController, presenter: HomePresenterMock)
    
    private func createSUT() -> Package {
        let presenter = HomePresenterMock()
        let sut = HomeViewController()
        sut.presenter = presenter
        _ = sut.view
        return Package(sut, presenter)
    }
}

extension HomeViewControllerTests {
    func test_buildSearch() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.buildSearch()
        // THEN
        XCTAssertTrue(package.sut.navigationItem.titleView is UISearchBar)
    }

    
    func test_reloadData_loaded() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.reloadData(forState: .loaded)
        // THEN
        XCTAssertNil(package.sut.tableView.backgroundView)
    }
    
    func test_applyFilters_noFilter() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.applyFilters([])
        // THEN
        XCTAssertTrue(package.presenter.isApplyFiltersCallStatus.isCalled)
        XCTAssertTrue(package.presenter.isApplyFiltersCallStatus.firstCallParam?.isEmpty ?? false)
    }
    
    func test_applyFilters_withFilters() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.applyFilters([.animals])
        // THEN
        XCTAssertTrue(package.presenter.isApplyFiltersCallStatus.isCalled)
        XCTAssertFalse(package.presenter.isApplyFiltersCallStatus.firstCallParam?.isEmpty ?? true)
    }
    
    func test_addCategoryChild() {
        // GIVEN
        let package = createSUT()
        // WHEN
        package.sut.addCategoryChild(UIViewController())
        // THEN
        XCTAssertFalse(package.sut.children.isEmpty)
    }
}
