//
//  HomePresenterTests.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import XCTest

class HomePresenterTests: XCTestCase {
    private typealias Package = (sut: HomePresenter, view: HomeViewControllerMock, manager: HomeManagerMock, dataSourceManager: HomeDataSourceManagerMock)
    
    private func createSUT(forceError: Bool = false, needFilters: Bool = false, needAds: Bool = false) -> Package {
        let view = HomeViewControllerMock()
        let manager = HomeManagerMock(forceError: forceError)
        
        
        var adsByCategory: [AdCategory : [Ad]]? = nil
        if needAds {
            let ad = Ad()
            ad.categoryId = .animals
            adsByCategory = [.animals : [ad]]
        }
        
        let dataSourceManager = HomeDataSourceManagerMock(with: adsByCategory,
                                                          filters: needFilters ? [.animals] : [])
        let sut = HomePresenter(with: view, manager: manager)
        sut.dataSourceManager = dataSourceManager
        return (sut: sut, view: view, manager: manager, dataSourceManager: dataSourceManager)
    }
}

extension HomePresenterTests {
    func test_didLoad_success() {
        // Given
        let package = createSUT()
        // When
        package.sut.didLoad()
        // Then
        XCTAssertTrue(package.view.isReloadDataCalled.isCalled)
        XCTAssertTrue(package.view.isReloadDataCalled.firstCallParam == .some(.loaded))
        XCTAssertTrue(package.manager.isLoadItemsCalled.isCalled)

    }
    
    func test_didLoad_failure() {
        // Given
        let package = createSUT(forceError: true)
        // When
        package.sut.didLoad()
        // Then
        XCTAssertTrue(package.view.isReloadDataCalled.isCalled)
        XCTAssertTrue(package.view.isReloadDataCalled.firstCallParam == .some(.error))
        XCTAssertTrue(package.manager.isLoadItemsCalled.isCalled)

    }
    
    func test_applyFilters_withNoFilter() {
        // Given
        let package = createSUT()
        // When
        package.sut.applyFilters([])
        // Then
        XCTAssertTrue(package.view.isReloadDataCalled.isCalled)
        XCTAssertTrue(package.sut.filters.isEmpty)
        XCTAssertTrue(package.view.isReloadDataCalled.firstCallParam == .some(.empty))
    }
    
    func test_applyFilters_withFilters() {
        // Given
        let package = createSUT(needFilters: true, needAds: true)
        // When
        package.sut.applyFilters([.animals])
        // Then
        XCTAssertTrue(package.view.isReloadDataCalled.isCalled)
        XCTAssertFalse(package.sut.filters.isEmpty)
        XCTAssertTrue(package.view.isReloadDataCalled.firstCallParam == .some(.loaded))
    }
    
    func test_numberOfRows_noData() {
        // Given
        let package = createSUT()
        // When
        let rowsCount = package.sut.numberOfRows(atSection: 0)
        // Then
        XCTAssertTrue(rowsCount == .zero)
    }
    
    func test_numberOfRows_withData() {
        // Given
        let package = createSUT(needAds: true)
        // When
        let rowsCount = package.sut.numberOfRows(atSection: 0)
        // Then
        XCTAssertTrue(rowsCount == 1)
    }
    
    func test_getFilters_noFilter() {
        // Given
        let package = createSUT()
        // When
        let filtersCount = package.sut.getFilters().count
        // Then
        XCTAssertTrue(filtersCount == .zero)
    }
    
    func test_getFilters_withFilters() {
        // Given
        let package = createSUT()
        package.sut.dataSourceManager?.filters = [AdCategory.animals, .booksCdDvd]
        // When
        let filtersCount = package.sut.getFilters().count
        // Then
        XCTAssertTrue(filtersCount == 2)
    }
}

