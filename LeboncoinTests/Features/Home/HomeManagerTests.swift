//
//  HomeManagerTests.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import XCTest

class HomeManagerTests: XCTestCase {
    // Create System under tests
    func createSUT(forceError: Bool = false) -> HomeManager {
        let sut = HomeManager()
        sut.serviceManager = ServiceManagerMock(forceError: forceError)
        return sut
    }
}

extension HomeManagerTests {
    func test_loadItems_success() {
        // Given
        let sut = createSUT()
        // When
        var isError = false
        let loadItemsExpectation = expectation(description: "loadItems expectation")
        sut.loadItems { _ in
            loadItemsExpectation.fulfill()
        } failure: { _ in
            isError = true
            loadItemsExpectation.fulfill()
        }
        waitForExpectation()
        // Then
        XCTAssertFalse(isError)
    }
    
    func test_loadItems_failure() {
        // Given
        let sut = createSUT(forceError: true)
        // When
        var isError = false
        let loadItemsExpectation = expectation(description: "loadItems expectation")
        sut.loadItems { _ in
            loadItemsExpectation.fulfill()
        } failure: { _ in
            isError = true
            loadItemsExpectation.fulfill()
        }
        waitForExpectation()
        // Then
        XCTAssertTrue(isError)
    }
}
