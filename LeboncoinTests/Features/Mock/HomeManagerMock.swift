//
//  HomeManagerMock.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

class HomeManagerMock: HomeManagerProtocol {
    // MARK: Properties
    var forceError = false
    var isLoadItemsCalled = CallStatus<Bool>.none
    
    // MARK: Init
    public init(forceError: Bool = false) {
        self.forceError = forceError
    }
    
    func loadItems(success: @escaping ([Ad]) -> Void, failure: @escaping (Error) -> Void) {
        forceError ? failure(ServiceError.missingData) : success([])
        isLoadItemsCalled.iterate()
    }
}
