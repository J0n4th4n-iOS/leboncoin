//
//  HomeDataSourceManagerMock.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 15/09/2021.
//

import Foundation
import XCTest

class HomeDataSourceManagerMock: HomeDataSourceManagerProtocol {
    var isSortAdsByCategoryCallStatus = CallStatus<[Ad]?>.none
    var isGetCategoryCallStatus = CallStatus<Int>.none
    var idGetCategoriesCallStatus = CallStatus<Never>.none
    var isGetAdsCallStatus = CallStatus<AdCategory>.none
    var isGetAdsSortedCallStatus = CallStatus<Never>.none
    var isUpdateFiltersAdsCallStatus = CallStatus<[AdCategory]>.none
    
    //MARK: Init
    init(with adsByCategory: [AdCategory: [Ad]]?, filters: [AdCategory]) {
        self.adsByCategory = adsByCategory
        self.filteredAds = adsByCategory
        self.filters = filters
    }
    
    var adsByCategory: [AdCategory: [Ad]]?
    var filters: [AdCategory]
    var filteredAds: [AdCategory : [Ad]]?
    
    func sortAdsByCategory(_ ads: [Ad]?) {
        isSortAdsByCategoryCallStatus.iterate(with: ads)
    }
    
    func getCategory(at index: Int) -> AdCategory? {
        isGetCategoryCallStatus.iterate(with: index)
        return getCategories()?.getOrNull(index)
    }
    
    func getCategories() -> [AdCategory]? {
        guard let adsByCategory = filteredAds else { return nil }
        
        return Array(adsByCategory.keys)
    }
    
    func getAds(for category: AdCategory) -> [Ad]? {
        isGetAdsCallStatus.iterate(with: category)
        return filteredAds?[category]
    }
    
    func getAdsSorted() -> [Ad]? {
        isGetAdsSortedCallStatus.iterate()
        return adsByCategory.map { $0.values.flatMap { $0 }}
    }
    
    func updateFiltersAds(with filters: [AdCategory]) {
        isUpdateFiltersAdsCallStatus.iterate(with: filters)
        filteredAds = adsByCategory
    }
}
