//
//  HomeViewControllerMock.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import UIKit

class HomeViewControllerMock: HomeViewProtocol, HomeViewCollectionDelegate {
    var isBuildSearchCalled = CallStatus<Never>.none
    var isReloadDataCalled = CallStatus<DataState>.none
    var isAddCategoryChild = CallStatus<UIViewController>.none
    
    func buildSearch() {
        isBuildSearchCalled.iterate()
    }
    
    func reloadData(forState state: DataState) {
        isReloadDataCalled.iterate(with: state)
    }
    
    func addCategoryChild(_ childViewController: UIViewController) {
        isAddCategoryChild.iterate(with: childViewController)
    }
}

