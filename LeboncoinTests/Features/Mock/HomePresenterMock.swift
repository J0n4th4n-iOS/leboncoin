//
//  HomePresenterMock.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

class HomePresenterMock: HomePresenterProtocol {
    // MARK: Properties
    var isDidLoadCallStatus = CallStatus<Never>.none
    var isNumberOfRowsCallStatus = CallStatus<Int>.none
    var isConfigureCellCallStatus = CallStatus<(AdCategoryCellProtocol, IndexPath)>.none
    var isGetAdsCallStatus = CallStatus<Never>.none
    var isApplyFiltersCallStatus = CallStatus<[AdCategory]>.none
    
    init() {
        self.filters = []
    }
    
    var filters: [AdCategory]
    
    func didLoad() {
        isDidLoadCallStatus.iterate()
    }
    
    func numberOfRows(atSection section: Int) -> Int {
        isNumberOfRowsCallStatus.iterate(with: section)
        return .zero
    }
    
    func configure(_ cell: AdCategoryCellProtocol, atIndexPath indexPath: IndexPath) {
        isConfigureCellCallStatus.iterate(with: (cell, indexPath))
    }
    
    func getAds() -> [Ad] {
        isGetAdsCallStatus.iterate()
        return []
    }
    
    func applyFilters(_ adCategories: [AdCategory]) {
        isApplyFiltersCallStatus.iterate(with: adCategories)
    }
}
