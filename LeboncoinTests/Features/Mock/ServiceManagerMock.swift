//
//  ServiceManagerMock.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

class ServiceManagerMock: ServiceManagerProtocol {
    // MARK: Properties
    var forceError: Bool = false
    // MARK: Init
    init(forceError: Bool) {
        self.forceError = forceError
    }
    
    func request(_ endpoint: Endpoint, completion: @escaping (Result) -> Void) {
        if forceError {
            completion(.failure(ServiceError.missingData))
        } else {
            var data: Data?
            switch endpoint {
            case .loadItems:
                data = JSONTools.load(fromFile: "ads", inBundle: Bundle(for: ServiceManagerMock.self))
            }
            
            guard let dataJson = data else {
                completion(.failure(ServiceError.missingData))
                return
            }
            DispatchQueue.main.async {
                completion(.success(dataJson))
            }
        }
    }
}

extension ServiceManagerMock {
    // MARK: Private methods
//    private func deserializeJSON<T: Decodable>(with data: Data?) -> Result<T> {
//        guard let data = data else { return .failure(ServiceError.missingData) }
//        
//        do {
//            let deserializedValue = try JSONDecoder().decode(T.self, from: data)
//            return .success(deserializedValue)
//        } catch let error {
//            return .failure(error)
//        }
//    }
}
