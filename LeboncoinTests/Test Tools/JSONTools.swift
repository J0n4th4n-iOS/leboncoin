//
//  JSONTools.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

enum ExtensionFile: String {
    case json
}

struct JSONTools {
    public static func load(fromFile: String, inBundle bundle: Bundle) -> Data? {
        if fromFile.contains(".json") {
            let file = String(fromFile.split(separator: ".").first ?? "")
            return Data.load(from: bundle, andFile: file, withType: .json, options: .mappedIfSafe)
        } else {
            return Data.load(from: bundle, andFile: fromFile, withType: .json, options: .mappedIfSafe)
        }
    }
}
