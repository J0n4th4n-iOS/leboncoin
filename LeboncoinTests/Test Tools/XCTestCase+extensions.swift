//
//  XCTestCase+extensions.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation
import XCTest

extension XCTestCase {
    func waitForExpectation(duration: TimeInterval = 10) {
        waitForExpectations(timeout: duration) { error in
            if let error = error {
                XCTFail("Expectation Error : \(error)")
            }
        }
    }
}
