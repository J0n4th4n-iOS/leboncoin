//
//  Data+Extensions.swift
//  LeboncoinTests
//
//  Created by Jonathan BACQUEY on 12/09/2021.
//

import Foundation

extension Data {
    static func load(from bundle: Bundle,
                     andFile file: String,
                     withType fileType: ExtensionFile,
                     options: Data.ReadingOptions = []) -> Data? {
        guard
            let path = bundle.path(forResource: file, ofType: fileType.rawValue),
            let data = try? Data(contentsOf: URL(fileURLWithPath: path), options: options)
        else {
            return nil
        }
        return data
    }
}
